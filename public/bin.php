<?php
 
function curl_post_request($url, $data) 
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $content = curl_exec($ch);
    curl_close($ch);
    return $content;
}
 
$postData = array(
    "user-id" => "sysapp",
    "api-key" => "zh5oz8IeW0uIl4hPomRZpH82bBpvvbPXjL1bmPghARnPNJND",
    "bin-number" => "434005 "
);
 
$json = curl_post_request("https://neutrinoapi.com/bin-lookup", $postData); 
$result = json_decode($json, true);
 
echo $result['valid']."\n";
echo $result['country-code']."\n";
echo $result['card-brand']."\n";
echo $result['card-type']."\n";
echo $result['card-category']."\n";
echo $result['issuer']."\n";
echo $result['issuer-website']."\n";
?>
 